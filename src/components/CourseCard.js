import { Row, Col, Button, Card } from 'react-bootstrap';

export default function CourseCard() {
    return (

        <Row className='mt-3 mb-3'>
            <Col>

                <Card className='cardHighlight p-3'>
                    <Card.Body>
                        <Card.Title>Sample Course</Card.Title>
                        <Card.Text>
                            Description: This is a sample course offering.
                        </Card.Text>
                        <Card.Text>
                            Price:  Php 40,000
                        </Card.Text>
                        <Button variant="primary">Enroll</Button>
                    </Card.Body>
                </Card>

            </Col>



        </Row>



    )
}